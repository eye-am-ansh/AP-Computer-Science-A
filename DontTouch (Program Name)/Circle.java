/**
 * Circle.java
 * Class that represents a Circle. 
 * Has properties of x / y coordinate (plotted on a Cartesian plane) and radius
 * @author Ansh Chaurasia
 * @version 1.0
 * @since 10/25/2019
 */


import java.awt.Color;
public class Circle
{
	private double x, y;
	private double radius;
	
	public Circle()
	{	
		x = y = radius = 0.0;
	}
	
	public Circle(double x, double y, double radius)
	{
		this.x = x;
		this.y = y;
		this.radius = radius;
	}
	
	public Circle(double x, double y)
	{
		this.x = x;
		this.y = y;
		setRandomRadius();
	}

	public void drawCircle()
	{
		StdDraw.setPenColor(new Color((int)(255 * Math.random()), (int)(255 * Math.random()), (int)(255 * Math.random()))); 
		StdDraw.filledCircle(x,y, radius);
		StdDraw.show();
	}
	
	public String toString()
	{
		return String.format("Circle Object Radius: %.2f X Coordinate: %.2f Y Coordinate: %.2f",radius,x,y);
	}

	public void setRandomRadius()
	{
		radius = Math.random() + 0.01;
	}
	
	public void setX(double x)
	{
		this.x = x;
	}

	public void setY(double y)
	{
		this.y = y;
	}

	public void setRadius(double radius)
	{
		this.radius = radius;
	}

	public double getX()
	{
		return x;
	}

	public double getY()
	{
		return y;
	}

	public double getRadius()
	{
		return radius;
	}
	
	public double getArea ( )
	{
		return (Math.PI * Math.pow(radius,2));
	}

	public double computeDistanceSum(double x, double y)
	{
		double dx = x - this.x; dx *= dx;
		double dy = y - this.y; dy *= dy;
		return Math.sqrt(dx + dy);
	}
	
	public boolean intersects(Circle other)
	{
		double dx = other.x - this.x;
		double dy = other.y - this.y;
		
		dx *= dx; dy *= dy;
		double distanceBetween = Math.sqrt(dy + dx);
		return distanceBetween <= (this.radius + other.radius);
	}

	public boolean checkPrematureIntersection(double cx, double cy, double radius)
	{
		return computeDistanceSum(cx,cy) <= (this.radius + radius);
	}
}



