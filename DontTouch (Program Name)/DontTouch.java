/**
 * DontTouch.java
 *
 * Code to pack 1000 circles 
 * with as much area as we can
 * @author Ansh Chaurasia
 * @version 1.0
 * @since 10/25/2019
 */

import java.awt.Color;
import java.awt.Font;

public class DontTouch
{
	/**  The array for the Circles to be drawn.                         */
	final int ARRAY_SIZE = 1000;
	Circle listOfCircles [];
	
	/**  Constructs the size (1000) for the array of Circles.           */
	public DontTouch ( )
	{
		listOfCircles = new Circle[ARRAY_SIZE];
	}
	
	public static void main(String [] args)
	{
		DontTouch run = new DontTouch();
		run.setUpCanvas();
		run.drawCircles();
		run.drawAxes();
		run.printArea();
	}
	
	/** 
	 *  Sets up the canvas, using methods from StdDraw.  This includes
	 *  setting up the canvas size, the horizontal scale (Xscale), and
	 *  the vertical scale (Yscale).  
	 */
	public void setUpCanvas ( )
	{
		final int WIDTH = 1200;
		final int HEIGHT = 800;
		StdDraw.setCanvasSize(WIDTH, HEIGHT);
		StdDraw.setXscale(-6, 6);
		StdDraw.setYscale(-4, 4);
		StdDraw.clear(new Color(255,255,255));
		
		StdDraw.enableDoubleBuffering();
	}
	
	/**
	 *  Creates the Circles in the array of Circles.  Draws the Circles.
	 */
	public void drawCircles ( )
	{
		solveUsingBruteForce();
		//optimizeSolve();
	}	

	public void optimizeSolve()
	{
		int circleCount = 0, r = 0;
		while(circleCount < 1000 && r < 100000)
		{
			r++;
			Circle optimal = new Circle(randX(), randY());
			//System.out.println(optimal.getX()  + " " + optimal.getY());
			if(!outOfBoundary(optimal))		
			{
				double bestRad = getOptimalRadius(optimal, circleCount); 
	
				if(bestRad <= 0.0315)
					optimal = new Circle(randX(),randY());
				else
				{
					listOfCircles[circleCount] = optimal;
					listOfCircles[circleCount].setRadius(bestRad);
					listOfCircles[circleCount++].drawCircle();
				}	
			}

			//if(r == 20)
				//return;

//			return;
		}


	}

	public double getOptimalRadius(Circle circle, int circleCount)
	{
		double l = 0.0, r = 1.0;

		while(l < r && (r - l) > 0.0001)
		{
			double mid = (l + r) / 2.0;

			//System.out.println(mid + " " + l + " " + r);

			if(!hasIntersectionWithOther(new Circle(circle.getX(), circle.getY(),mid), circleCount))
				l = mid;
			else
				r = mid;

		}
		return l;
	}

	public void solveUsingBruteForce()
	{
		double radius = 1.0;
		int circleCount = 0, j = 0;

		while(circleCount < 1000)
		{
			for(int i = 0; i < 500 && circleCount < 1000; i++)
			{
				Circle rCircle = new Circle(randX(),randY(),radius);
				if(!hasIntersectionWithOther(rCircle, circleCount))
				{
					listOfCircles[circleCount] = rCircle;
					rCircle.drawCircle();
					circleCount++;
				}
			}
			radius = 0.9922229 *radius;
		}
	}

	public double randX()
	{
		double rand = (Math.random() * 12) - 6;
		return rand;
	}
	
	public double randY()
	{
		double rand = (Math.random() * 8) - 4;
		return rand;
	}

	public boolean outOfBoundary(Circle x)
	{
		return (x.getX() + x.getRadius()  > 6 || x.getX() - x.getRadius() < -6) || 
		(x.getY() + x.getRadius()  > 4 || x.getY() - x.getRadius() < -4);
	}
	
	public boolean hasIntersectionWithOther(Circle x, int index)
	{
		for(int i = 0; i < index && listOfCircles[i] != null; i++)
			if(x.intersects(listOfCircles[i]))
			{
				return true;
			}
		if(outOfBoundary(x))
			return true;
		return false;
	}
	
	
	
	
	/**
	 *  Draws a pair of axes, over the drawn Circles.  Grid lines are drawn and
	 *  the scale is shown, to help the viewer see the size of the Circles.
	 */
	public void drawAxes ( )
	{
		Font font = new Font("Arial", Font.PLAIN, 18);
		StdDraw.setFont(font);
		StdDraw.setPenColor(new Color(220,220,220)); 
		for(double integers = -6; integers <= 6; integers++)
		{
			StdDraw.line(integers,-4,integers,4);
			StdDraw.line(-6,integers,6,integers);
			StdDraw.setPenColor(new Color(0,0,0)); 	
			StdDraw.text(integers,-0.4,"" + (int)integers);
			StdDraw.text(-0.3,integers-0.05,"" + (int)integers);
		}
		StdDraw.show();
	}

	/**
	 *  Adds the area of each circle to a total area.  Prints this total 
	 *  area to the terminal window.
	 */
	public void printArea ( )
	{	
		double area = 0.0;
		for(int i = 0; i < listOfCircles.length && listOfCircles[i] != null; i++)
			area += listOfCircles[i].getArea();
		
		System.out.println("\n\n\nTotal Area: " + area + "\n\n\n");
	}
}
