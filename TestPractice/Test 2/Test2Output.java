/**
 * Test2Ouput
 */
public class Test2Output 
{
    public static void main(String[] args)
    {
        new Test2Output().run();       
    }

    public void run()
    {
        printVers();
    }

    public void solveOutput1()
    {
        for(int row = 1; row <= 5; row++)
        {
            int col = 1;
            while(col <= 5)
            {
                if(row == col || row + col == 5)
                {
                    System.out.print(row);
                }
                else
                {
                    System.out.print("*");
                }
                col++;
            }
            System.out.println();
        }    
    }
    
    public void solveOutput2() 
    {
        int x = 2;
        int number = 3;
        int value = 10;

        while(value < 18)
        {
            if(value % number == 0)
            {
                value++;
                number++;
            }
            else if(value % x == 0)
            {
                value++;
                x++;
            }
            else
            {
                number--;
                x--;
            }
            value++;
            System.out.println(x + " " + number + " " + value);
        }
    }

    public void solveMathProblems() 
    {
        System.out.println((int)(Math.PI * 100) == 314); //Problem 1
        System.out.println(89 / 23 + 67 - 16 % 17 * 4); //Problem 2
        System.out.println(!(65 > 4 * 15) || false); //Problem 3
        System.out.println(!(8 / 7.8 > 1 && true) || (int)7.4 < 7.4 ); //Problem 4
        System.out.println(65 * 10.0 > 1324 / 4 && 30 / 9 < 3.0 || !true); //Problem 5
        
    }

    public void parseStringValues()
    {
        String x = "Haa I'm a muffin. And it's muffin time. Who wants a muffin? Please I want to derp      sadfasdfs ";

        
        while(x.indexOf(" ") != -1)
        {
            int end = x.indexOf(" ");
            String subsString = x.substring(0,end);
            System.out.println(subsString);

            x = x.substring(end + 1).trim();
        }

        if(x.length() != 0)
            System.out.println(x);

    }

    public void createLines()
    {
        int x [] = {1,2,3,4,5,6,7,8,9,10};
        int y [] = {1,2,3,4,5,6,7,8,9,10};

        int x1 = 0,y1 = 0, x2 = x[0], y2 = y[0];
        int i = 1;

        while(i != x.length)
        {
            x1 = x[i];
            y1 = y[i];
            System.out.println("Line from Prev: " + x2 + " " + y2 + " to " + x1 + " " + y1);
            x2 = x1;
            y2 = y1;

            i++;
        }
    }

    public void getInputNumberMatchStyle()
    {
        int val = 1234;
        int val2 = 4321;

        int master [] = process(val);
        int player [] = process(val2);

        System.out.println(getPartialMatches(master,player));
    }

    public int getPartialMatches(int [] master, int [] player)
    {
        int exactMatches = 0,ret = 0;
        for(int i = 0; i < 4; i++)
            if(master[i] == player[i])
                exactMatches++;

        for(int i = 1; i <= 9; i++)
        {
            int cnt = countOcc(master,i);
            int cnt2 = countOcc(player,i);
            ret += Math.min(cnt2,cnt);
        }
        ret -= exactMatches;
        return ret;
    }

    public int countOcc(int arrs [], int searchFor)
    {
        int cnt = 0;
        for(int i = 0; i < arrs.length; i++)
            if(arrs[i] == searchFor)
                cnt++;
        return cnt;
    }

    public int [] process(int x)
    {
        int array [] = new int[4];
        int i = 0;
        while(x != 0)
        {
            array[i] = x % 10;
            x /= 10;
            i++;
        }
        return array;
    }

    public void printVers()
    {
        int val = 1213;
        String str = val + "";
        for(int i = 0; i < str.length(); i++)
             System.out.print(str.substring(i) + " ");
        System.out.println();
        for(int i = str.length(); i > 0; i--)
             System.out.print(str.substring(0,i) + " ");
 
     }

}