/**
 * TestClasses
 */
public class TestClasses 
{
    public static void main(String[] args) {
        new TestClasses().testClasses();
    }

    public void testClasses()
    {
        test(new Walker());
    }

    public void test(myInterface mInterface)
    {
        System.out.println(mInterface.whoAmI());
    }
}

abstract class Biped 
{
    String x = "Biped";
    public abstract void firstStep();
    
    public String getX()
    {
        return x;
    }

}

class Walker extends Biped implements myInterface
{
    String x = "Walker";
    public Walker()
    {
        System.out.println("Walker Constructor");
    }

    public void firstStep()
    {
        System.out.println("YEEET");
    }

    public String getX()
    {
        return x;
    }

    public String whatAmI(String inp)
    {
        return inp;
    }

    public String whoAmI()
    {
        return "TRROOP TROOP 4933333";
    }
}

class Hopper extends Biped
{
    String x = "Hopper";
    public Hopper()
    {
        System.out.println("Hopper Constructor");
    }

    public void firstStep()
    {
        System.out.println("Hopper First Step");
    }


    public String getX()
    {
        return x;
    }


}

interface myInterface
{
    String whatAmI(String inp);
    String whoAmI();
}