import java.util.ArrayList;


/**
 * PT1
 */
public class PT1 {

    public static void main(String[] args) {
        new PT1().run();
    }

    public void run()
    {
//        solveAL1();
//        solveAL2();
//        solveAL3();
//        solveAL4();
          solveAL5();
    }

    public void solveAL1()
    {
        ArrayList <String> mv = new ArrayList<String>();
        mv.add("freshmen");
        mv.add(1,"sophomores");
        mv.add(1,"juniors");
        mv.add(1,"seniors");
        System.out.println("\n\n" + mv + "\n\n");
    }

    public void solveAL2()
    {
        String [] fishy = {"One", "fish", "two", "fish", "red", "fish", "blue", "fish"};
        ArrayList <String> rhyme = new ArrayList <String> ();
        for (int i = 0; i < fishy.length; i++)
            rhyme.add(fishy[i]);
        int index = rhyme.indexOf("fish");
        for (int k = 0; k < 4; k++)
            rhyme.remove(index + k);
        System.out.println("\n\n" + rhyme + "\n\n");
    }

    public void solveAL3()
    {
        ArrayList <Double> values = new ArrayList <Double> ();
        for (int k = 1; k <= 5; k++)
            values.add(k + 0.5);                                                                                    
        for (int i = 0; i < values.size(); i++)
        {
            int b = values.size() - 1 - i;
            Double temp = values.get(i);
            values.set(i, values.get(b));
            values.set(b, temp);
        }
        System.out.println("\n\n" + values + "\n\n");
    }

    public void solveAL4()
    {
        ArrayList <Integer> numbers = new ArrayList <Integer> ();
        for (int value = 1; value <= 5; value++)
             numbers.add(value * value - 1);                                                              
        for (int i = 0; i < numbers.size(); i++)
            if (numbers.get(i).intValue() % 3 == 0)
                numbers.remove(i);
        System.out.println("\n\n" + numbers + "\n\n");
    }

    public void solveAL5()
    {
        ArrayList <Integer> mylist = new ArrayList <Integer> ();
        mylist.add(7);                                                                                                              
        mylist.add(8);
        mylist.add(9);
        int len = mylist.size();
        for (int i = 0; i < len; i++)
        {
            mylist.add(i + 1, new Integer(i));
            mylist.set(i, new Integer(i + 2));
        }
        System.out.println("\n\n" + mylist + "\n\n");
    }
}