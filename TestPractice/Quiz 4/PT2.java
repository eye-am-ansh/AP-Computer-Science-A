

/**
 * PT2
 */
import java.util.*;
 public class PT2 
{

    public static void main(String[] args) {
        new PT2().run();
    }

    public void run()
    {
        //solveAL1();
        //solveAL2();
//      System.out.println(workonList(6));
        //solveAL3();
        //solveAL4();
        errorTest1();
}

    public void solveAL1()
    {
        ArrayList<String> myclasses = new ArrayList<String>();
   
        myclasses.add("AP Computer Science");
        myclasses.add("World History");
        myclasses.add("American Literature");
        myclasses.add(0, "AP Biology");
        myclasses.remove(3);
        myclasses.add(0, "Physical Education");
        System.out.println(myclasses);
    }

    public void solveAL2()
    {
        ArrayList<Integer> al = new ArrayList<Integer>(Arrays.asList(0,1,2,3,4,5,6));
        swap1(new ArrayList<Integer>(al));
        swap2(new ArrayList<Integer>(al));
        swap3(new ArrayList<Integer>(al));

    }

    public void swap1(ArrayList <Integer> number)
    {
        Integer temp = number.get(5);       
        number.remove(5);         
        number.add(5,number.get(0));       
        number.remove(0);          
        number.add(0,temp);
        System.out.println(number);
    }

    public void swap2(ArrayList <Integer> number)
    {
        Integer temp = number.get(5);
        number.set(5,number.get(0));
        number.set(0,temp);
        System.out.println(number);
    }

    public void swap3(ArrayList <Integer> number)
    {
        Integer temp1 = number.get(5);
        Integer temp2 = number.get(0);
        number.set(0,temp1);
        number.set(5,temp2);
        System.out.println(number);
    }

    public ArrayList <Integer> workonList (int num)
    {
        ArrayList <Integer> sequence = new ArrayList <Integer> ();
        for (int i = 1; i <= num; i++)
        sequence.add(new Integer(i * i + 2));
        return sequence;
    }

    public void solveAL3()
    {
        ArrayList <Integer> numbers = new ArrayList<>(Arrays.asList(1,3,9,14,18));
        for (int i = 0; i < numbers.size(); i++)
            if (numbers.get(i).intValue() % 3 == 0)
                numbers.remove(i);
        System.out.println(numbers);
    }

    public void solveAL4()
    {
        ArrayList<Integer> list = new ArrayList<Integer>();
        list.add(2);
        list.add(1);
        list.add(0);
        int n = list.size();
        for (int i = 0; i < n; i++)
        {
            int value = list.get(i);
            if (value > 0)
            list.add(0,value);
        }
        System.out.println(list);
    }

    public void errorTest1()
    {
        ArrayList<String> stringList = new ArrayList<String>();
        ArrayList<int> intList = new ArrayList<int>();
        ArrayList<Comparable> compList = new ArrayList<Comparable>();
    }
}