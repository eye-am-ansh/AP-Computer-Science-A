public class Scope
{
    private int number;

    public Scope ( )
    {
        int number = 5;
    }

    public static void main(String [] args)
    {
        Scope run = new Scope(); run.methods();
    }

    public void methods ( )
    {
        method1(number);
        System.out.println(number);
        number += 10;
        method2();
        System.out.println(number);
    }

    public void method1(int number)
    {
        System.out.println(number);
        number += 15;
        System.out.println("Here" + number);
    }

    public void method2 ( )
    {
        System.out.println(number);
        number += 20;
        System.out.println(number);
    }
}