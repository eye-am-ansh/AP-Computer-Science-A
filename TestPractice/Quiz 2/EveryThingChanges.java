public class EveryThingChanges
{

    public static void main(String[] args)
    {
        EveryThingChanges run = new EveryThingChanges();
        run.changeThem();
    }

    public void changeThem ( ) {
        double[] array = {2.0, 5.0, 9.0};
        printArray(array);
        changeArrayOne(array);
        printArray(array);
        changeArrayTwo(array);
        printArray(array);
        String name = new String("FRED");
        changeString(name);
        System.out.println(name);
    }
    public void printArray(double [] arr)
    {
        System.out.println(arr[0] + " " + arr[1] + " " + arr[2]);
    }

    public void changeArrayOne(double [] arr) {
        arr[0] = arr[1];
        arr[1] = arr[2];
        arr[2] = arr[0];
        printArray(arr);
    }
    public void changeArrayTwo(double [] arr)
    {
        arr = new double[3];
        arr[0] = 1.1;
        arr[1] = 2.2;
        arr[2] = 3.3;
        printArray(arr);
    }

    public void changeString(String n) {
        n += n;
        System.out.println(n);
    }
}


