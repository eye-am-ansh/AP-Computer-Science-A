public class QuizQs
{
    public static void main(String[] args)
    {
//        question1();
//        arithmeticQuestions();
        traceLoop();
    }

    public static void question1() //5 points
    {
        int ahab = 5;
        int moby = 6;
        int stubb = 11;

        do
        {
            if(moby % ahab > 2)
                ahab++;
            else if (moby % ahab < 2)
            {
                ahab--;
                moby++;
            }
            if(stubb / ahab == 2)
                stubb--;
            else
            {
                stubb++;
                moby += 2;
            }
            System.out.println(ahab + " " + moby + " " + stubb);
        } while (stubb / moby >= 1);
    }

    public static void arithmeticQuestions () //Two Points
    {
        System.out.println(67 / 8 % 7 * 2 % 3 * 6 - 3); //Question 1
        System.out.println(35.7 >= 7 + 59 / 7 % 12 * 4); //Q2
        System.out.println(35 + 12 % 13 / 5 * 7 - 13); //Q3
        System.out.println((double)68 / 3 != (double)(68 / 3)); //Q4
    }

    public static void traceLoop()
    {
        for (int row = 1; row <= 5; row++)
        {
            int col = 1;

            while (col <= 5)
            {
                if (row % col == 1)
                {
                    System.out.print(col);
                } else
                {
                    System.out.print("*");
                }

                col++;
            }

            System.out.println();
        }
    }



}

