public class PointTester
{
    public static void main(String[] args) 
    {
        Point pnt1 = new Point(2, 4);
        Point pnt2 = new Point(6, 8);
        System.out.println("\n\nX: " + pnt1.getX() + " Y: " + pnt1.getY());     
        System.out.println("X: " + pnt2.getX() + " Y: " + pnt2.getY());     
        tricky(pnt1,pnt2);
        System.out.println("\nX: " + pnt1.getX() + " Y: " + pnt1.getY());     
        System.out.println("X: " + pnt2.getX() + " Y: " + pnt2.getY() + "\n\n");       
    }

    public static void tricky(Point arg1, Point arg2)
    {
        arg1.setX(20);
        arg1.setY(40);
        Point temp = arg1;
        arg1 = arg2;
        arg2 = temp;
        System.out.println("X: " + arg1.getX() + " Y: " + arg1.getY());     
        System.out.println("X: " + arg2.getX() + " Y: " + arg2.getY());     


    }
}

class Point
{
    private int xval;
    private int yval;

    public Point(int a, int b)
    {
        xval = a;
        yval = b;
    }

    public void setX(int x)
    {
        xval = x;
    }

    public void setY(int y)
    {
        yval = y;
    }

    public int getX()
    {
        return xval;
    }
    
    public int getY()
    {
        return yval;
    }
    
}