public class MoreParameters
{
    public static void main(String[] args) 
    {
        new MoreParameters().changeArray();
    }

    public void changeArray()
    {
        int [] array = {5,12,27};
        printArray(array);
        changeArrayOne(array);
        printArray(array);
        changeArrayTwo(array);
        printArray(array);
    }

    public void printArray(int [] arr)
    {
        System.out.println(arr[0] + " " + arr[1] + " " + arr[2]);
    }

    public void changeArrayOne(int [] arr)
    {
        arr = new int [3];
        arr[0] = 1;
        arr[1] = 2;
        arr[2] = 3;
        printArray(arr);
    }

    public void changeArrayTwo(int [] arr)
    {
        arr[0] = arr[1];
        arr[1] = arr[2];
        arr[2] = arr[0];
        printArray(arr);
    }
}