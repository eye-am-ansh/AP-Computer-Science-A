public class ArrayParameters
{
    public static void main(String[] args) 
    {
        ArrayParameters arrayParameters = new ArrayParameters();
        arrayParameters.methods();     
    }

    public void methods()
    {
        System.out.println("\n\n\n");
        char [] cool = {'J','A','V','A'};
        char [] neat = {'C','O','D','E'};
        processOne(cool, neat); printArray(cool); printArray(neat);
        processTwo(cool, neat); printArray(cool); printArray(neat);
        processThree(cool, neat); printArray(cool); printArray(neat);
        System.out.println("\n\n\n");
    }

    public void printArray(char [] a)
    {
        for(int i = 0; i < a.length; i++)
        {
            System.out.print(" " + a[i]);
        }
        System.out.println("\n");
    }

    public void processOne(char [] A1, char [] A2)
    {
        A1 = A2 = new char [] {'C','O','M','P','U','T','E','R'};
        printArray(A1);
        printArray(A2);
    }

    public void processTwo(char [] arrA, char [] arrB)
    {
        arrA[0] = 'L'; arrA[1] = 'O'; arrA[2] = 'V'; arrA[3] = 'E';
        arrB[0] = 'A'; arrB[1] = 'P'; arrB[2] = 'C'; arrB[3] = 'S';
        printArray(arrA);
        printArray(arrB);
    }

    public void processThree(char [] a, char [] b)
    {
        char [] temp = a;
        a = b;
        b = temp;
        printArray(a);
        printArray(b); 
    }
}