public class test1
{
    public static void main(String[] args) 
    {
        new test1().run();        
    }
    public void run()
    {
        System.out.println(relativelyPrime(8,21));
    }

    public boolean relativelyPrime(int x, int y)
    {
        if(x == y)
            return y == 1;
        return relativelyPrime(Math.max(x, y) - Math.min(x,y), Math.min(x, y));
    }

    public int max1020(int a, int b)
    {
        if(inRange(a) && inRange(b))
            return Math.max(a,b); 
        
        if(inRange(a))
            return a;
        if(inRange(b))
            return b;
        return 0;
    }

    public boolean inRange(int v) 
    {
        return v >= 10 && v <= 20;
    }

}