/**
 * InterfacesTest
 */
public class InterfacesTest implements fruit,tool
{
    public void apple()
    {
        System.out.println("Hello");
    }

    public void pear()
    {
        System.out.println("Hello");
    }

    public void pinapple()
    {
        System.out.println("Hello");
    }

    public void pencil()
    {
        System.out.println("Hello");
    }

    public void pen()
    {
        System.out.println("Hello");
    }

    public void eraser()
    {
        System.out.println("Hello");
    }

    public static void main(String[] args) {
        new InterfacesTest().testInts();
    }
    
    public void testInts()
    {
        paramInt(this,this);
    }

    public void paramInt(tool t, fruit f)
    {
        t.pencil();
        f.apple();
    }

}

interface fruit
{
    abstract void apple();
    abstract void pear();
    abstract void pinapple();
}

interface tool
{
    abstract void pencil();
    abstract void pen();
    abstract void eraser();
}