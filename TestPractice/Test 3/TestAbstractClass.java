
/**
 * TestAbstractClass
 */
public class TestAbstractClass extends Abstract2
{
    public static void main(String[] args) {
        new TestAbstractClass().testAbstract();
    }

    public void testAbstract()
    {
        te();
        //testAb(this);
    }

    public void testAb(Abstract1 test1)
    {
        test1.printAb();
    }

    public void printAb()
    {
        System.out.println("HIIII22222I");
    }


    public void m1()
    {
        System.out.println("M1 CALLED");
    }

    public void m2()
    {
        System.out.println("M2 CALLED");
    }

}


abstract class Abstract1 implements i1 
{
    public Abstract1()
    {
        System.out.println("Abstract Constructor");
    }

    public abstract void printAb();
    
}

abstract class Abstract2 extends Abstract1 implements i2, i1
{
    public Abstract2()
    {
        System.out.println("Abstract 2 Constructor");
    }

    public abstract void printAb();

    public void te()
    {
        printAb();
        printAb();
    }
}

interface i1
{
    void m1();
}

interface i2
{
    void m2();
}