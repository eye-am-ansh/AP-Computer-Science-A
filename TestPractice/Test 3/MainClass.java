class P
{
    public static void st()
    {
        
        System.out.println("Static method p");
    }
}

class A extends P
{
    public int i = 10;
    public A()
    {
    }

    public static void st()
    {
        System.out.println("Static method hkjhk");
    }

    public  void im()
    {
        super.st();
        st();
        System.out.println("Instance Method");
    }

    public void d()
    {
        System.out.println("B Obj");
    }
}
 
class B extends A
{
    public int i = 20;
    public B()
    {

    }
    public static void st()
    {        
        System.out.println("Static method B");


    }

    public void d()
    {
        System.out.println("D Obj");
    }

    public void im()
    {
        super.im();
        System.out.println("Instance Method B");
    }
}
 
public class MainClass
{
    public static void main(String[] args)
    {
        B a = new B();
        a.im();

    }
}