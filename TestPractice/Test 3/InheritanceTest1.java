/**
 * InheritanceTest1
 */
public class InheritanceTest1 {
    public static void main(String[] args) 
    {
        //SonClass sc = new MomClass();
        MomClass mc = new SonClass();
        System.out.println(mc.var);

        //Question 1
 //       sc.instanceMethod();
//        mc.instanceMethod();

        //System.out.println();
        //Question 2
        //sc.staticMethod();
        //mc.staticMethod();
    }
     
}

class MomClass
{
    int var;
    public MomClass()
    {
        System.out.println("Mom Constructor");
        var = -1;
    }
    public static void staticMethod() {
        System.out.println("MomClass staticMethod");
    }
    public void instanceMethod() 
    {
        staticMethod();
        System.out.println("MomClass instanceMethod");
    }
}

class SonClass extends MomClass{

    public SonClass()
    {
        System.out.println("Son Constructor");
        var = -2;
    }

    public static void staticMethod() {
       
        //super.staticMethod();
        System.out.println("SonClass staticMethod");
    }

    public void instanceMethod() 
    {
//        super.instanceMethod();
        System.out.println("SonClass instanceMethod");
    }
}