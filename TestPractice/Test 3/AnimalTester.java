public class AnimalTester
{
	public static void main(String[] args)
	{
		Dog rover = new Dog();
		Animal myAnimal = rover;
		myAnimal.testClassMethod();
		myAnimal.testInstanceMethod();
	}
}

class Animal
{
    private String x = "I AM AN ANIMAL";
	public static void testClassMethod()
	{
		System.out.println("The class method in Animal.");
	}
	
	public void testInstanceMethod()
	{
		System.out.println("The instance method in Animal.");
	}
}

class Dog extends Animal
{
	public static void testClassMethod()
	{
		System.out.println("The class method in Dog.");
	}
	
	public void testInstanceMethod()
	{
		System.out.println("The instance method in Dog.");
    }
    
    public Dog test(Animal g)
    {
        System.out.println(g.x);
    }
}
