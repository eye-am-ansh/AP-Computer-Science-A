public  class ReplaceValues
{
    public static void main(String[] args)
    {
        int [] array = {2,3,4,5,4,3,4,4,8};
		printArray("Original: ", array);
        replaceValues(array, 4, 7);

		System.out.println("Replace 4 with 7");
        printArray("After   : ", array);
        System.out.println();
        
		array = new int [] {1,2,3,4,5,4,3,2};
		printArray("Original: ", array);
		replaceValues(array, 3, 9);
		System.out.println("Replace 3 with 9");
		printArray("After   : ", array);
		System.out.println();
		array = new int [] {2,2,1,2,1,2,2};
		printArray("Original: ", array);
		replaceValues(array, 2, 5);
		System.out.println("Replace 2 with 5");
		printArray("After   : ", array);
		System.out.println();
		array = new int [] {3,3,3};
		printArray("Original: ", array);
		replaceValues(array, 3, 1);
		System.out.println("Replace 3 with 1");
		printArray("After   : ", array);
		System.out.println();
		array = new int [] {1,2,1,2,1,2};
		printArray("Original: ", array);
		replaceValues(array, 1, 2);
		System.out.println("Replace 1 with 2");
		printArray("After   : ", array);
		System.out.println();
		array = new int [] {1};
		printArray("Original: ", array);
		replaceValues(array, 1, 2);
		System.out.println("Replace 1 with 2");
		printArray("After   : ", array);
		System.out.println();
		array = new int [] {};
		printArray("Original: ", array);
		replaceValues(array, 1, 2);
		System.out.println("Replace 1 with 2");
		printArray("After   : ", array);
    }

    public static void replaceValues(int [] array, int target, int replacer)
    {
        for(int i = 1; i < array.length - 1; i++)
        {
            if(array[i] == target && (array[i] != array[i + 1] && array[i] != array[i - 1]))
                array[i] = replacer;
        }
        if(array.length == 0)
            return;
        if(array.length == 1)
        {
            if(array[0] == target)
                array[0] = replacer;
            return;
        }
        if(array[0] == target && array[0] != array[1])
            array[0] = replacer;
        if(array[array.length - 1] == target && array[array.length - 2] != array[array.length - 1])
            array[array.length - 1] = replacer;
    }

	public static void printArray(String str, int [] a)
	{
		System.out.print(str);
		for(int i = 0; i < a.length; i++)
		{
			System.out.print(a[i] + "  ");
		}
		System.out.println();
	}
}