import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;

/**
 *	Prompt.java - Uses BufferedReader.
 *	Provides utilities for user input.  This enhances the BufferedReader
 *	class so our programs can recover from "bad" input, and also provides
 *	a way to limit numerical input to a range of values.
 *
 *	The advantages of BufferedReader are speed, synchronization, and piping
 *	data in Linux.
 *
 *	@author	your name
 *	@since	date
 */

public class Prompt
{
	/**  Variables for reading in the information from the keyboard.   */
    private static InputStreamReader streamReader = new InputStreamReader(System.in);
    private static BufferedReader inputReader = new BufferedReader(streamReader);
	
	
	/**
	 *	Prompts user for string of characters and returns the string.
	 *	@param ask  The prompt line
	 *	@return  	The string input
	 */
	public static String getString (String ask)
	{

        String result = "";

        try 
        {
            System.out.print(ask + ": ");
            result = inputReader.readLine();            
        } catch (Exception e) 
        {
            System.err.println("Error in reading Text Input.");
        }

		return result;
	}
	
	/**
	 *  Prompts the user and picks up an int.  Checks for
	 *  "bad" input and reprompts if not an int.
	 *  @param ask       The String prompt to be displayed to the user.
	 *  @return          The int entered by the user.
	 */
	public static int getInt (String ask)
	{
        boolean badInput = true;
        int result = 0;
        do
        {
            badInput = false;
            String input = getString(ask);
            try
            {
                result = Integer.parseInt(input);
            }
            catch(Exception e)
            {
                badInput = true;
            }
        }
        while(badInput);

		return result;
	}
	
	/**
	 *  Prompts the user and picks up an int.  Checks for
	 *  "bad" input and reprompts if not an int.  Also checks
	 *  for input within a given range, and reprompts if outside
	 *  that range.
	 *  @param ask       The String prompt to be displayed to the user.
	 *  @param min       The minimum integer value to be allowed as input.
	 *  @param max       The maximum integer value to be allowed as input.
	 *  @return          The int entered by the user.
	 */
	public static int getInt (String ask, int min, int max)
	{
        boolean badInput = true;
        int result = 0;

        do
        {
            try
            {
                badInput = false;
                result = getInt(ask + " from (" + min + " to " + max + ")");
                if(!(result >= min && result <= max))
                    badInput = true;
            }
            catch(Exception e)
            {
                badInput = true;
            }
        }
        while(badInput);
		return result;
	}
	
	/**
	 *	Prompts the user for a character and returns the character.
	 *	@param ask  The prompt line
	 *	@return  	The character input
	 */
	public static char getChar (String ask)
	{
        boolean badInput = true;
        char result = ' ';
        do
        {
            String input = getString(ask);
            try 
            {
                badInput = false;
                result = input.charAt(0);
                if(input.length() != 1)
                    badInput = true;    
            }
            catch (Exception e) 
            {
                badInput = true;
            }
        }
        while(badInput);

		return result;
	}
    
    	/**
	 *	Prompts the user for a character and returns the character.
     *	@param ask  The prompt line
     *  @param possible The possible values of the character
	 *	@return  	The character input
	 */
	public static char getChar (String ask, char [] possible)
	{
        boolean badInput = true;
        char result = ' ';
        do
        {
            try 
            {
                badInput = true;
                result = getChar(ask);
                for(int i = 0; i < possible.length && badInput; i++)
                    if(possible[i] == result)
                        badInput = false;
            }
            catch (Exception e) 
            {
                badInput = true;
            }
        }
        while(badInput);

		return result;
	}

	/**
	 *	Prompts the user for a double and returns the double.
	 *	@param ask  The prompt line
	 *	@return  The double input
	 */
	public static double getDouble (String ask)
	{
        boolean badInput = true;
        double result = 0.0;
        do
        {
            badInput = false;
            String input = getString(ask);
            try
            {
                result = Double.parseDouble(input);
            }
            catch(Exception e)
            {
                badInput = true;
            }
        }
        while(badInput);

		return result;
	}
	
	/**
	 *	Prompts the user for a double and returns the double.
	 *	@param ask  The prompt line
	 *	@param min  The minimum double accepted
	 *	@param max  The maximum double accepted
	 *	@return  The double input
	 */
	public static double getDouble (String ask, double min, double max)
	{
        boolean badInput = true;
        double result = 0.0;
        do
        {
            try
            {
                badInput = false;
                String formatted = String.format("(from %.2f to %.2f)",min,max);
                result = getDouble(ask + formatted);
                if(!(result >= min && result <= max))
                    badInput = true;
            }
            catch(Exception e)
            {
                badInput = true;
            }
        }
        while(badInput);
		return result;
    }
}