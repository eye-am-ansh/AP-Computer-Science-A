public class TestInput
{
	public static void main (String [] args)
	{
		TestInput run = new TestInput();
		run.getInts();
		run.getDoubles();
		run.getChars();
	}
	
	public void getInts ( )
	{
		System.out.println("\n\n\n");
		int value1 = Prompt.getInt("Please enter any integer");
		int value2 = Prompt.getInt("Please enter an age value",12,75);
		System.out.println("\n\n" + value1 + "    " + value2 + "\n");
	}
	
	public void getDoubles ( )
	{
		double value1 = Prompt.getDouble("Please enter any double");
		double value2 = Prompt.getDouble("Please enter a weight",5.0,400.0);
		System.out.printf("%n%n%10.2f%10.2f%n%n",value1,value2);
	}
	
	public void getChars ( )
	{
		char mychar = Prompt.getChar("Please enter any char");
		char [] possible = {'f','F','m','M'};
		char gender = Prompt.getChar("Please enter a character for your gender (M or F)", possible);
		System.out.println("\n\n" + mychar + "    " + gender);
		System.out.println("\n\n\n");
	}
}

/*


C:\Java\Jackpot>java TestInput




Please enter any integer: Fred
Please enter any integer: five
Please enter any integer:
Please enter any integer: Johnny
Please enter any integer:
Please enter any integer: alnbabw
Please enter any integer: 23
Please enter an age value (from 12 to 75): Fred
Please enter an age value (from 12 to 75): Six
Please enter an age value (from 12 to 75): seven
Please enter an age value (from 12 to 75):
Please enter an age value (from 12 to 75):
Please enter an age value (from 12 to 75): 11
Please enter an age value (from 12 to 75): 76
Please enter an age value (from 12 to 75): 12


23    12

Please enter any double: Francis
Please enter any double: jimmy
Please enter any double:
Please enter any double: dhalwgliauwe
Please enter any double: 45.23
Please enter a weight (from 5.00 to 400.00): s;ndaabn
Please enter a weight (from 5.00 to 400.00): 4.99
Please enter a weight (from 5.00 to 400.00): 400.0001
Please enter a weight (from 5.00 to 400.00): 63


     45.23     63.00

Please enter any char: fred
Please enter any char: fr
Please enter any char:
Please enter any char: 34
Please enter any char: a;sgdhaiuogha;
Please enter any char: X
Please enter a character for your gender (M or F): fred
Please enter a character for your gender (M or F): jill
Please enter a character for your gender (M or F): mf
Please enter a character for your gender (M or F): Mf
Please enter a character for your gender (M or F): m


X    m





C:\Java\Jackpot>

*/