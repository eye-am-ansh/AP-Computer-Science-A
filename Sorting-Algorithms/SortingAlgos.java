import java.util.Arrays;

/**
 * SortingAlgos
 */
public class SortingAlgos 
{
    public static void main(String[] args)
    {
        new SortingAlgos().run();    
    }

    public void run()
    {
        int [] unsorted = {1000,4000,2,3,6,7,9};
        //bubbleSort(unsorted);
        mergeSort(0,unsorted.length - 1,unsorted);
        printArray(unsorted);
    }

    public void printArray(int [] a)
    {
        for(int i = 0; i < a.length; i++)
            System.out.print(a[i] + " ");
        System.out.println();
    }

    public void bubbleSort(int [] a)
    {
        for(int outer = 0; outer < a.length - 1; outer++)
            for(int inner = 0; inner < a.length - outer - 1; inner++)
                if(a[inner] > a[inner + 1])
                {
                    int temp = a[inner];
                    a[inner] =  a[inner + 1];
                    a[inner + 1] = temp;
                }
    
    }
    
    public void selectionSort(int [] a)
    {
        int mx = 0, temp = 0;
        for(int i = a.length; i > 1; i--)
        {
            mx = 0;
            for(int j = i; j >= 0; j--)
                if(a[j] > a[mx])
                    mx = j;
            temp = a[mx];
            a[mx] = a[i];
            a[i] = temp;
        }
    }

    public void insertionSort(int [] a)
    {
        int temp;
        for(int i = 1; i < a.length; i++)
        {
            temp = a[i];
            int j = i;
            while(j > 0 && temp < a[j - 1])
            {
                a[j] = a[j - 1];
                j--;
            }
            a[j] = temp;
        }
    }

    public void mergeSort( int l, int r, int a[])
    {
        if(count(l,r) <= 2)
        {
            if(count(l,r) == 2 && a[l] > a[r])
                swap(a,l,r);
            return;
        }
        int mid = (l + r) >> 1;

        mergeSort(l, mid,a);
        mergeSort(mid + 1, r, a);
        mergeIntervals(l,mid,r,a);
    }

    public int count(int a, int b)
    {
        return  b - a + 1;
    }

    public void mergeIntervals(int l, int m, int r, int a [])
    {
        int [] left = Arrays.copyOfRange(a, l, m + 1);
        int [] right = Arrays.copyOfRange(a, m + 1, r + 1);  
        
        for(int i = l, lp = 0, rp = 0; i <= r; i++)
        {
            if((rp == right.length) || (lp < left.length && left[lp] < right[rp]) )
                a[i] = left[lp++];
            else
                a[i] = right[rp++];
        }

    }

    public void swap(int [] a, int l, int r)
    {
        a[l] ^= a[r];
        a[r] ^= a[l];
        a[l] ^= a[r];
    }
}