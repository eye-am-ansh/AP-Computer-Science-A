/*
  Implementation Completed by Ansh Chaurasia
   This class implements a vendor that sells one kind of items.
   A vendor carries out sales transactions.
*/

public class Vendor
{
  // Fields:
  private int priceOfSnack,numberOfItems,totalMoney;


  /**
   * @param itemPrice The price of an item
   * @param numberOfItems The number of the items
   */
  public Vendor (int itemPrice, int numberItems)
  {
      priceOfSnack = itemPrice;
      numberOfItems = numberItems;
      totalMoney = 0;
  }

  //  Sets the quantity of items in stock.
  //  Parameters:
  //    int number of items to place in stock
  //  Return:
  //    None
  public void setStock (int amount)
  {
      numberOfItems = amount;
  }

  //  Returns the number of items currently in stock.
  //  Parameters:
  //    None
  //  Return:
  //    int number of items currently in stock
  public int getStock ()
  {
      return numberOfItems;
  }

  //  Adds a specified amount (in cents) to the deposited amount.
  //  Parameters:
  //    int number of cents to add to the deposit
  //  Return:
  //    None
  public void addMoney (int numCents)
  {
      totalMoney += numCents;
  }

  //  Returns the currently deposited amount (in cents).
  //  Parameters:
  //    None
  //  Return:
  //    int number of cents in the current deposit
  public int getDeposit ()
  {
      return totalMoney;
  }

  //  Implements a sale.  If there are items in stock and
  //  the deposited amount is greater than or equal to
  //  the single item price, then adjusts the stock
  //  and calculates and sets change and returns true;
  //  otherwise refunds the whole deposit (moves it into change)
  //  and returns false.
  //  Parameters:
  //    None
  //  Return:
  //    boolean successful sale (true) or failure (false)
  public boolean makeSale ()
  {
    if(totalMoney >= priceOfSnack && numberOfItems > 0)
    {
        totalMoney -= priceOfSnack;
        numberOfItems--;
        return true;
    }
    return false;
  }

  //  Returns and zeroes out the amount of change (from the last
  //  sale or refund).
  //  Parameters:
  //    None
  //  Return:
  //    int number of cents in the current change
  public int getChange ()
  {
      return totalMoney;
  }
}
