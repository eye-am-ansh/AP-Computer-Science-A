import java.util.Scanner;

/**
 * NumberMatch.java
 * Finds the matches between two numbers. If the numbers are the same, we count them as partials. We allow the user to continuosly guess the value, until the correct value is found
 * @author Ansh Chaurasia
 * @version 1.0
 * @since 9/24/2019
 */

public class NumberMatch
{
	/**  The int array containing the random "master" key. These are to be filled with random numbers from 1 to 9.  */
	private int [] master;
	
	/**  The int array used to store the user's guesses.  */
	private int [] guess;
	
	/**  If this boolean is set to true, then the master key should be shown for every turn.  Otherwise the master is not shown until the end. */
	private boolean show;

	
	public NumberMatch ( )
	{
        show = false;
        guess  = new int []{0,0,0,0,0,0,0,0,0,0};
        master  = new int []{0,0,0,0,0,0,0,0,0,0};

        //  Set up the field variables.
	}
	
	/**
	 *  Sets up and runs NumberMatch.
	 *  @param  args     An array of String arguments (the first element,
	 *                   at index 0, could be used.  If the user enters
	 *                   "show" then the MASTER KEY should be printed
	 *                   before every user turn).
	 */
	public static void main(String [] args)
	{		
		NumberMatch run = new NumberMatch();
		if(args.length > 0)
		{
			run.setShow(args[0]);
		}
		run.instructions();
		run.play();
	}
    
    
	public void setShow(String str)
	{
		if(str.equalsIgnoreCase("show"))
		{
			show = true;
		}
	}	
	
	public void instructions ( )
	{
		System.out.println("\n\n\n");
		System.out.println("   _   _                 _                 __  __       _       _  ");      
		System.out.println("  | \\ | |_   _ _ __ ___ | |__   ___ _ __  |  \\/  | __ _| |_ ___| |__  "); 
		System.out.println("  |  \\| | | | | '_ ` _ \\| '_ \\ / _ \\ '__| | |\\/| |/ _` | __/ __| '_ \\ ");
		System.out.println("  | |\\  | |_| | | | | | | |_) |  __/ |    | |  | | (_| | || (__| | | |");
		System.out.println("  |_| \\_|\\__,_|_| |_| |_|_.__/ \\___|_|    |_|  |_|\\__,_|\\__\\___|_| |_|\n");
		System.out.println("Welcome to the game of NUMBERMATCH (TM).  In this game, an array of 4 ints (the master)");
		System.out.println("is filled with random numbers from 1 to 9.  The goal of the game is to guess these random");
		System.out.println("numbers, in order.  The user is prompted to enter a 4-digit number, and the input is");
		System.out.println("checked to make sure it is within the range from 1000 to 9999, and contains no 0's.  Then,");
		System.out.println("the user is informed of the number of exact digit matches, and the number of partial digit");
		System.out.println("matches.  For these matches, exact + partial may not exceed 4, because a match may be exact");
		System.out.println("or it may be partial, but it cannot be both.  The user continues to make guesses until four");
		System.out.println("exact matches are made.  Once the puzzle is solved, the game ends, with an exit message,");
		System.out.println("informing the user of how many guesses it took to find the pattern. See the sample run");
		System.out.println("outputs for detailed formatting information.");
		System.out.println("\n  THE MASTER KEY HAS BEEN CHOSEN.  LET THE GAME BEGIN . . .\n\n\n");
	}
    
    /**
     * Plays the game
     */
	public void play ( )
	{
        
        Scanner tempInput = new Scanner(System.in); //Temporary Source of input

        //Dice rand  = new Dice(1000);
        int playerGuess = -1, computerValue = (int)(Math.random() * 9999 + 1000),count = 0;
        boolean badInput = true;
        String prompt = "Please enter an integer value, with no zero digits (from 1000 to 9999):";

        master  = toDigitForm(computerValue);
        do
        {
            try 
            {
                if(show)
                    System.out.println("HERE IS THE MASTER KEY: " + computerValue);

                badInput = false;

                System.out.print(prompt);
                playerGuess = tempInput.nextInt(); //FIX INPUT HERE
                
                
                
                ++count;
                guess = toDigitForm(playerGuess);

                /*System.out.println("Printing Master: ");
                printArray(master);

                System.out.println("Printing Guess: ");
                printArray(guess); */

                badInput = checkExit(master,guess, playerGuess,computerValue);

            } 
            catch (Exception e) 
            {

            }
        }
        while(badInput);
        exitMessage(count,computerValue);

		//  This method should have a loop, and it should call the methods necessary to
		//  play the game.  This method should not be too long.
	}
    
    /**
     * Checks if the game is over. 
     * @param computer An occurance array for the computer values
     * @param player An occurance array for the player values
     * @param playerNumber The player's guessed value
     * @param computerNumber The computer's number that the player is trying to guess
     * @return A boolean. True if the game is over, False otherwise
     */
    public boolean checkExit(int computer [], int player [], int playerNumber, int computerNumber) 
    {
        if(playerNumber == computerNumber) //The number is same? No need to proceed. Else, check for partials
        {
            printStats(playerNumber,4,0);
            return false;
        }
            int partials = 0,exacts = 0, temp = playerNumber;
        
        for(int i = 0; i < 4; i++)
        {
            if(playerNumber % 10 == computerNumber % 10)
                ++exacts;
            playerNumber /= 10;
            computerNumber /= 10;
        }

        for (int i = 0; i < 10; i++)
            if(computer[i] > 0 && player[i] > 0)
                partials += Math.min(computer[i], player[i]);
        partials -= exacts; //Subtract overcounting
        printStats(playerNumber,partials,exacts);

        return true;
    }
    
    /**
     * Prints out the statistics after each attempt by the user.
     * @param guess An integer representing the player's guess
     * @param partials An integer representing the number of partials the player received
     * @param exacts An integer representing the number of exacts the player received
     */
    public void printStats(int guess, int partials, int exacts)
    {
        System.out.printf("%n%-30s:%5d%n","YOUR GUESS",guess);
        System.out.printf("%-30s:%5d%n","Exact Matches",exacts);
        System.out.printf("%-30s:%5d%n%n%n","Partial Matches",partials);
    }

    /**
     * Converts the number into a 9 digit array - with each value representing the # of times the digit (0 - 9) has appeared
     * @param value The initial value to be converted into occurrance-array format.
     * @return An Integer Array of Size 10, with the corresponding occurances for the number
     */
    public int [] toDigitForm(int value) 
    {
        int ret [] = new int [10];
        for (int i = 0; i < 4; i++)
        {
            int lastDigit = value % 10;
            ++ret[lastDigit];
            value /= 10;
        }        
        return ret;
    }

    /**
     * Prints the final goodbye to the user before terminating the program. 
     * Converts the number into a string for quick and easy printing. 
     * @param count The number of attempts it took the user to get the number
     * @param computer The computer's number (that the player had to guess)
     */
	public void exitMessage(int count, int computer)
	{
        String stringVers = computer + "";
    
        System.out.println("Congratulations! You found the master key. ");
		System.out.println("\n    +-------------+");
		System.out.println("    |   " + stringVers.charAt(0) + " " + stringVers.charAt(1) + " " + stringVers.charAt(2) + " " + stringVers.charAt(3) + "   |");
		System.out.println("    +-------------+\n");
        System.out.println("It took you " + count + " guesses to find it\nGoodbye for now, and please play again...");		
	}
}