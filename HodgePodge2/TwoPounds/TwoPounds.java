/**
 * Ansh Chaurasia
 * Finds total combinations of pound 2 using Knapsack DP
 * Date: 10/1/19
 */

public class TwoPounds
{

   public static void main(String[] args) 
   {
       new TwoPounds().run();         
   }

   public void run()
    {
        int dp [] = new int [2500];

        int types [] = {1,2,5,10,20,50,100,200};

        dp[0] = 1;
        for(int i = 0; i < types.length; i++)
            for(int j = 0; j <= 200; j++)
                if(dp[j] > 0)
                    dp[j + types[i]] += dp[j];

        System.out.println("The number of ways to have 2 pounds is : " + dp[200]); 
    }

 

    
}