/**
 * Ansh Chaurasia
 * Program that finds all "truncatable" primes in a given range
 * Date: 10/1/19
 */

 public class Truncatable
 {
    private final int N = 100000;
    public static void main(String[] args) 
    {
        new Truncatable().run();         
    }

    public void run()
    {
        int truncCount =  0, sum = 0;
        for(int i = 10; truncCount < 11; i++)
            if(isTruncatable(i))
            {
                //System.out.println("Found Truncatable: " + i);
                sum += i;
                truncCount++;
            }


        System.out.println("Sum of the 11 Truncatable Integers: " + sum); 
    }

    public boolean isTruncatable(int i)
    {
        //System.out.println("In isTruncatable");
        String strVers = i + "";

        String temp1 = strVers, temp2 = strVers;
        while(temp1.length() != 0)
        {
            if(!isPrime(Integer.parseInt(temp1)))
                return false;
            if(temp1.length() != 1)
                temp1 = temp1.substring(1);
            else 
                temp1 = "";
        }

        while(temp2.length() != 0)
        {
            if(!isPrime(Integer.parseInt(temp2)))
                return false;
            temp2 = temp2.substring(0,temp2.length() - 1);
        }
        return true;
    }

    public boolean isPrime(int val)
    {
        if(val == 1)
            return false;
        for(int i = 2; i <= Math.sqrt(val); i++)
            if(val % i == 0)
                return false;
        return true;
    }
 }