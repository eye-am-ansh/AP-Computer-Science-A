/**
 * Ansh Chaurasia
 * Finds the sum of all amicable values
 * An Amicable value is any, where f(f(x)) = x, and f(x) returns the sum of the divisors of x
 * Date: 10/1/19
 */

public class AmicableSum
{
    private int TOTAL = 10000;   
   public static void main(String[] args) 
   {
       new AmicableSum().run();         
   }

   public void run()
    {
        int sum = 0;
        int total [] = new int [15000];

        for(int i = 1; i <= TOTAL; i++)
            total[i] = getDivisorSum(i);
        for(int i = 1; i <= TOTAL; i++)
            if(total[i] <= TOTAL && total[total[i]] == i)
                sum += i;
        System.out.println("Total Sum of Amicable Numbers: " + sum);
    }

    public int getDivisorSum(int val)
    {
        int divSum = 0;
        for(int i = 1; i < val; i++)
            if(val % i == 0)
                divSum += i;
        return divSum;
    }

    
}