/**
 * Ansh Chaurasia
 * Program that finds max pythagorean triples value p
 * Date: 10/1/19
 */

public class MostTriples
{
   private final int MAX_P = 1000;
   public static void main(String[] args) 
   {
       new MostTriples().run();         
   }

   public void run()
    {
        int best = 0,bestP = 0;
        for(int p_val = 1; p_val <= MAX_P; p_val++)
        {
            int total = getTotalSets(p_val);
            if(total > best)
            {
                best = total;
                bestP = p_val;
            }
        } 

//        System.out.println("Total Sets for p = 120: " + getTotalSets(120));
       System.out.println("The Best P Value is: " + bestP + " with a triple count of " + best + "!!!"); 
    }

    public int getTotalSets(int pValue)
    {
        int totalSets = 0;
        for(int a = 1; a <= pValue; a++)
            for(int b = 1; b <= a; b++)
            {
                int c = pValue - (a + b);
            
                if(b * b + c * c == a * a &&  a + b + c == pValue && c > 0)
                    totalSets++;
            }
        return totalSets / 2;
    }

    
}