/**
 * Ansh Chaurasia
 * Finds the sum of all amicable values
 * An Amicable value is any, where f(f(x)) = x, and f(x) returns the sum of the divisors of x
 * Date: 10/1/19
 */

public class InCircles
{
    private int TOTAL = 100000;   
   public static void main(String[] args) 
   {
       new InCircles().run();         
   }

   public void run()
   {
       int counter = 0;
        for(int i = 10; i <= TOTAL; i++)
            if(isCircularPrime(i))
            {
                counter++;
            }  
        System.out.println("The total number of circular primes are: " + (counter + 4));
   }

   public boolean isCircularPrime(int val)
   {    
       String stringVers = val + "";
        int cnt = 0;

        while(!stringVers.equals(val + "") || cnt == 0)
        {
            cnt++;
            if(!isPrime(Integer.parseInt(stringVers)))
                return false;
            stringVers = stringVers.charAt(stringVers.length() - 1) + stringVers.substring(0,stringVers.length() - 1);
        }

        return true;
   }

   public boolean isPrime(int val)
   {
       for(int i = 2; i <= Math.sqrt(val); i++)
           if(val % i == 0)
               return false;
       return true;
   }
    


}