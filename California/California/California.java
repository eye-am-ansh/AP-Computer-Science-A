
/**
 * California.java
 *
 * Program that prints a map of California. The program reads a text file called "california.txt" and "californiaborder.txt"
 * which has lists of cities. The program then plots a dot at it's approximate location. 
 *
 * @author Ansh Chaurasia
 * @version 1.0
 * @since 9/9/2019
 */

import java.awt.Color;
import java.awt.Font;
import java.util.Scanner;

public class California
{ 
	/** Creates a California object.
	 */
	public California ( )
	{ }

	/**
	 *  The main method.
	 */
	public static void main(String[] args)
	{
		California run = new California();
		run.setUpCanvas();
		run.drawBorder();
		run.drawCaliforniaCities();
	} 
	
	/** 
	 *  Sets up the canvas, using methods from StdDraw.  This includes
	 *  setting up the canvas size, the horizontal scale (Xscale), and
	 *  the vertical scale (Yscale).  We will enable double buffering,
	 *  in anticipation of running an animation.
	 */
	public void setUpCanvas ( )               //  This method is complete.
	{
		StdDraw.setCanvasSize(620, 700);
		StdDraw.setXscale(-125.0, -114.0);    //  Related to the longitude
		StdDraw.setYscale(32.0, 42.5);        //  Related to the latitude
		
		StdDraw.enableDoubleBuffering();
		StdDraw.setPenColor(StdDraw.DARK_GRAY); 
	}
	
	/** 
	 *  Using OpenFile, this method opens the text file californiaborder.txt.
	 *  Each line in the text file is an ordered pair (doubles) that represents
	 *  a point on the California border.  These points are read in, using a loop,
	 *  and the points are connected to form the border of California.  Don't
	 *  forget to close the text file!
	 */
	public void drawBorder ( )
	{
		try
		{
			
			Scanner sc = OpenFile.openToRead("californiaborder.txt");

			double x1 = 0.0, x2 = 0.0, y1 = 0.0, y2 = 0.0;

			String [] input = sc.nextLine().split(", ");
			x2 = Double.parseDouble(input[0]);
			y2 = Double.parseDouble(input[1]);			

			while(sc.hasNextLine())
			{
				input = sc.nextLine().split(", ");
				x1 = Double.parseDouble(input[0]);
				y1 = Double.parseDouble(input[1]);			

				StdDraw.line(x1,y1,x2,y2);
				x2 = x1;
				y2 = y1;
			}

			x1 = sc.nextInt();
			System.out.println(x1);
			
			StdDraw.setPenRadius(0.0025);
			double long1 = -123.7;
			double lat1 = 33.1;
			double long2 = -115.7;
			double lat2 = 40.8;
		
			StdDraw.line(long1,lat1,long2,lat2);
			StdDraw.show();
			sc.close();
		}
		catch(Exception e)
		{
			
			
		}	
	}
	
	/** 
	 *  Using OpenFile, this method opens the text file california.txt.  Each line
	 *  in the text file provides information about a California city.  After the
	 *  sixth comma the latitude for the city is provided (a double), and after the
	 *  seventh comma the longitude for the city is provided (a double).  These
	 *  values should be parsed as doubles, and the city should be represented as a
	 *  dot on the map.  Don't forget to close the text file.
	 *  
	 */
	public void drawCaliforniaCities ( )
	{
		try
		{
			StdDraw.show();
			Scanner sc = OpenFile.openToRead("california.txt");
		
			while(sc.hasNextLine())
			{
				StdDraw.setPenColor(StdDraw.DARK_GRAY); 
				StdDraw.setPenRadius(0.01);
				String input [] = sc.nextLine().split(",");
				double x1 = Double.parseDouble(input[6]); //Coordinates Reversed?
				double y1 = Double.parseDouble(input[7]);
				StdDraw.point(y1,x1);
				StdDraw.show(); 

				
			}
		}
		catch(Exception ex)
		{
		}
	}
} 

