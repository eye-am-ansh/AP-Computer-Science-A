/**
 * California2.java
 *
 * This is the second version of California. Modifying our code, we want to add a "fancier" GUI, with a time lapse of all the cities showing
 * all the cities. Cities with > 400,000 population would indicated in red.
 * @author Ansh Chaurasia
 * @version 1.0
 * @since 9/12/2019
 */

import java.awt.Color;
import java.awt.Font;
import java.util.Scanner;

public class California2
{ 
	/** Creates a California2 object.
	 */
	public California2 ( )
	{ }

	/**
	 *  The main method. 
	 *  1. Sets up the Canvas
	 *  2. Draws the Border
	 *  3. Creates the Legend
	 *  4. Draws all the cities
	 */
	public static void main(String[] args)
	{
		California2 run = new California2();
		run.setUpCanvas();
		run.drawBorder();
		run.legend();
		run.drawCaliforniaCities();
	} 
	
	/** 
	 *  Sets up the canvas, using methods from StdDraw.  This includes
	 *  setting up the canvas size, the horizontal scale (Xscale), and
	 *  the vertical scale (Yscale).  We will enable double buffering,
	 *  in anticipation of running an animation.
	 */
	public void setUpCanvas ( )               //  This method is complete.
	{
		StdDraw.setCanvasSize(620, 700);
		StdDraw.setXscale(-125.0, -114.0);    //  Related to the longitude
		StdDraw.setYscale(32.0, 42.5);        //  Related to the latitude
		
		StdDraw.enableDoubleBuffering();
		StdDraw.setPenColor(StdDraw.DARK_GRAY); 
	}
	
	/** 
	 *  Using OpenFile, this method opens the text file californiaborder.txt.
	 *  Each line in the text file is an ordered pair (doubles) that represents
	 *  a point on the California border.  These points are read in, using a loop,
	 *  and the points are connected to form the border of California.  Don't
	 *  forget to close the text file!
	 */
	public void drawBorder ( ) //Taken from "California.java" and modified.
	{
		try
		{
			
			Scanner sc = OpenFile.openToRead("californiaborder.txt");

			double x1 = 0.0, x2 = 0.0, y1 = 0.0, y2 = 0.0;

			String [] input = sc.nextLine().split(", ");
			x2 = Double.parseDouble(input[0]);
			y2 = Double.parseDouble(input[1]);			

			while(sc.hasNextLine())
			{
				input = sc.nextLine().split(", ");
				
		
				x1 = Double.parseDouble(input[0]);
				y1 = Double.parseDouble(input[1]);			

				StdDraw.line(x1,y1,x2,y2);
				StdDraw.show();
				StdDraw.pause(30);
				x2 = x1;
				y2 = y1;
			}

			x1 = sc.nextInt();
			//System.out.println(x1);
			
			StdDraw.setPenRadius(0.0025);
			double long1 = -123.7;
			double lat1 = 33.1;
			double long2 = -115.7;
			double lat2 = 40.8;
		
			StdDraw.line(long1,lat1,long2,lat2);
			sc.close();
		}
		catch(Exception e)
		{
			
			
		}	
	}
	
	/** 
	 *  Draws a legend in the upper right-hand corner of the output.
	 *  The legend is labeled "POPULATION" and shows the city size
	 *  for population ranges.  Cities of size greater than 400,000
	 *  are shown with a red border.  All other cites have a black
	 *  border.
	 */
	public void legend ( )					//  This method is complete.
	{		
		StdDraw.setPenColor(StdDraw.BLACK); 
		StdDraw.filledRectangle(-116.5,40,1.4,2);
		StdDraw.setPenColor(StdDraw.WHITE); 
		StdDraw.filledRectangle(-116.5,40,1.35,1.95);
		int population1 = 2000000, population2 = 500000;
				
		StdDraw.setPenColor(StdDraw.RED); 

		for(int i = 0; i < 4; i++)
		{
			StdDraw.setPenRadius(Math.pow(Math.log(population1/(Math.pow(10,i))),3.5) * 0.000005);
			StdDraw.point(-117.2,41.15 - 0.8 * i);
			StdDraw.setPenRadius(Math.pow(Math.log(population2/(Math.pow(10,i))),3.5) * 0.000005);
			StdDraw.point(-117.2,40.75 - 0.8 * i);
			StdDraw.setPenColor(StdDraw.BLACK); 
			StdDraw.setPenRadius(Math.pow(Math.log(population1/(Math.pow(10,i))),3.4) * 0.000005);
			StdDraw.point(-117.2,41.15 - 0.8 * i);
			StdDraw.setPenRadius(Math.pow(Math.log(population2/(Math.pow(10,i))),3.4) * 0.000005);
			StdDraw.point(-117.2,40.75 - 0.8 * i);
			StdDraw.setPenColor(StdDraw.WHITE); 
			StdDraw.setPenRadius(Math.pow(Math.log(population1/(Math.pow(10,i))),3.3) * 0.000005);
			StdDraw.point(-117.2,41.15 - 0.8 * i);
			StdDraw.setPenRadius(Math.pow(Math.log(population2/(Math.pow(10,i))),3.3) * 0.000005);
			StdDraw.point(-117.2,40.75 - 0.8 * i);
			StdDraw.setPenColor(StdDraw.BLUE); 
		}

		Font font = new Font("Arial", Font.BOLD, 18);
		StdDraw.setFont(font);
		StdDraw.setPenColor(StdDraw.BLACK); 
		StdDraw.textLeft(-117.5,41.6,"POPULATION");
		StdDraw.textLeft(-116.8,41.1,"2,000,000");
		StdDraw.textLeft(-116.8,40.7,"500,000");
		StdDraw.textLeft(-116.8,40.3,"200,000");
		StdDraw.textLeft(-116.8,39.9,"50,000");
		StdDraw.textLeft(-116.8,39.5,"20,000");
		StdDraw.textLeft(-116.8,39.1,"5,000");
		StdDraw.textLeft(-116.8,38.7,"2,000");
		StdDraw.textLeft(-116.8,38.3,"500");


	}
	
	/** 
	 *  Creates all of the cities.
	 *  Initially, the bottom left corner box representing the timeline is drawn.
	 *  Next, we look in californiacities.txt to find a city, and california.txt for it's coordinates, along with population
	 *  Finally, we call the createPoint() helper method and update the time (from the file)
	 *  
	 */
	public void drawCaliforniaCities ( )
	{
		StdDraw.setPenColor(StdDraw.BLACK); 
		StdDraw.filledRectangle(-122.8,33.6,1.1,0.6);
		StdDraw.setPenColor(StdDraw.WHITE); 
		StdDraw.filledRectangle(-122.8,33.6,1.05,0.55);
		Font font = new Font("Arial", Font.BOLD, 40);
		StdDraw.setFont(font);
		StdDraw.setPenColor(StdDraw.BLACK);
		StdDraw.text(-122.8,33.5,"");	

		StdDraw.setFont(new Font("Arial", Font.BOLD,18));
		Scanner in;
		try
		{
			in = OpenFile.openToRead("californiacities.txt");
		
			while(in.hasNextLine())
			{
				
				StdDraw.setPenColor(StdDraw.DARK_GRAY); 
				StdDraw.setPenRadius(0.01);
				StdDraw.setFont(new Font("Arial",Font.BOLD, 18));
				
				String printInput = in.nextLine();
				String input [] = printInput.split(",");
				
				
				for(int i = 0; i < input.length; i++)
				{
//					System.out.println(input[i]);
					input[i] = input[i].trim();
				}
//				System.out.println("\n\n");
			//	System.out.println("HERE: " + input[0] + " " + Integer.parseInt(input[2]));

				String date = input[input.length - 1];

				plotPoint(input[0], Integer.parseInt(input[2]));
				
				StdDraw.setPenColor(StdDraw.BLACK); 
				StdDraw.filledRectangle(-122.8,33.6,1.1,0.6);
				StdDraw.setPenColor(StdDraw.WHITE); 
				StdDraw.filledRectangle(-122.8,33.6,1.05,0.55);
				font = new Font("Arial", Font.BOLD, 40);
				StdDraw.setFont(font);
				StdDraw.setPenColor(StdDraw.BLACK);
				StdDraw.text(-122.8,33.5,date);	
				StdDraw.show();
				//StdDraw.pause(30);
  
			}
			in.close();

		}
		catch(Exception e)
		{
			
			
		}
		
	
		
		StdDraw.show();
		StdDraw.pause(30); 
	}
	
	/** 
	 * Plots a point onto the canvas. 
	 * 
	 * @param in The name of the city we are searching for (Ex: Los Angeles)
	 * @param population The population of the city plotted
	 * @return An Error code. If the method returns 0, the point was successfully plotted. Else, an exception was thrown
	 * @throws Exception We are trying to catch any file related exception - generalized to Exception
	 */
	public int plotPoint(String in, int population)
	{
		Scanner in2;
		try
		{	
			in2 = OpenFile.openToRead("california.txt");
			
			while(in2.hasNextLine())
			{
				String [] inp = in2.nextLine().split(",");

				for(int i = 0; i < inp.length; i++)
					inp[i] = inp[i].trim();

				if(inp[0].equals(in))
				{
					double lon = Double.parseDouble(inp[7]);
					double lat = Double.parseDouble(inp[6]);
					
					if(population >= 400000)
						StdDraw.textRight(lon - 0.1,lat - 0.2,inp[0]);

					createPoint(lon,lat, population);
					
					return 0; //Exited Properly
				}
			}	
			in2.close();
		}
		catch(Exception e)
		{	
			System.err.println("Fail");
		}
		
		return -1; //Error Code
	}
	
	
	/**
	 * Helper method that plots a point on the canvas using StdDraw. If the population is greater than 400,000 , the color is set to red. Else, it is set to Blue 
	 * @param x The 'x' coordinate of the point (Cartesian Coordinate System)
	 * @param y The 'y' coordinate of the point
	 * @param population The population to determine the color of the point
	 */
	public void createPoint(double x, double y, int population) //Helper Method to create a point
	{
		Color c;
		if(population <= 400000)
			c = StdDraw.BLUE;
		else
			c = StdDraw.RED;

		StdDraw.setPenColor(c);
		StdDraw.setPenRadius(Math.pow(Math.log(population),3.5) * 0.000005);
		StdDraw.point(x,y);
		StdDraw.setPenColor(StdDraw.BLACK); 
		StdDraw.setPenRadius(Math.pow(Math.log(population),3.4) * 0.000005);
		StdDraw.point(x,y);
		StdDraw.setPenColor(StdDraw.WHITE); 
		StdDraw.setPenRadius(Math.pow(Math.log(population),3.3) * 0.000005);
		StdDraw.point(x,y);
	}
	
} 
