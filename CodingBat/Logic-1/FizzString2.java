public String fizzString2(int n) 
{
  String ret = "";
  if(n % 3 == 0)
    ret += "Fizz";
  if(n % 5 == 0)
    ret += "Buzz";
  ret += "!";
  if(ret.length() != 1)
    return ret;
  
  return n + "!";
}
