public int teenSum(int a, int b) 
{
    if(isTeen(a) || isTeen(b))
      return 19;
    return a + b;
}

public boolean isTeen(int a)
{
  return 13 <= a && a <= 19;
}