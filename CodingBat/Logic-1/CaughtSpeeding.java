public int caughtSpeeding(int speed, boolean isBirthday) 
{
    int up = 81, lo = 61;
    if(isBirthday)
    {
      up += 5;
      lo += 5;
    }
    if(lo <= speed && speed <= up)
      return 1;
    if(speed > up)
      return 2;
    return 0;
}
