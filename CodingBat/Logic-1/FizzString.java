public String fizzString(String str)
{
  if(str.length() == 0)
    return "";
  
  String ret = "";
  if(str.charAt(0) == 'f')
    ret += "Fizz";
  if(str.charAt(str.length() - 1) == 'b')
    ret += "Buzz";
  if(ret.length() == 0)
    return str;
  
  return ret;
}
