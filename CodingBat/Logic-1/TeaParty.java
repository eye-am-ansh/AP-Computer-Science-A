public int teaParty(int tea, int candy) 
{
    if(tea < 5 || candy < 5)
      return 0;
    if(candy  >= tea * 2 || tea >= candy * 2)
      return 2;
    return 1;
}
