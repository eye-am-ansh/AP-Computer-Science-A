public boolean cigarParty(int cigars, boolean isWeekend) 
{
    if(isWeekend)
      return (cigars >= 40);
    return cigars <= 60 && cigars >= 40;
}
