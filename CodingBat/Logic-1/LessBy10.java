public boolean lessBy10(int a, int b, int c) {
    return isLess(a,b) || isLess(b,c) || isLess(a,c);
}

public boolean isLess(int a, int b)
{
  return Math.abs(a - b) >= 10;
}