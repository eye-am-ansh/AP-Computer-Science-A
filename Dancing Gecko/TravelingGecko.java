/**
 * TravelingGecko.java
 * 
 * @author Scott DeRuiter, Ansh Chaurasia
 * @version 1.0
 * @since 11/21/2019
 */

import java.awt.Color;
public class TravelingGecko extends DancingGecko implements Leader
{
	/**Represents the 1 gecko that will follow this TravelingGecko */
	private DancingGecko follower;

	/**Represents the gecko's state of direction */
	private boolean walkingForwards;


	/**
	 * Regular Constructor
	 * @param x X-Position
	 * @param y Y-Position
	 * @param c Color of the Background
	 */
	public TravelingGecko(int x, int y, Color c)
	{
		super(x,y,c);
		setStringLabel("L");
		init();
	}
	
	/**
	 * Regular Constructor
	 * @param x X-Position
	 * @param y Y-Position
	 * @param c Color of the Background
	 * @param p The point that represents direction of the gecko
	 */
	public TravelingGecko(int x, int y, Color c, Point p)
	{
		super(x,y,c,p);
		setStringLabel("L");
		init();
	}
	
	/**
	 * "Upon Initialization" method to set any necessary variables and to prevent repeated code
	 */
	public void init()
	{
		walkingForwards = true;
	}
	
	/**
	 * @return Point representing the location of gecko
	 */
	public Point getLocation()
	{
		return new Point(super.getX(),super.getY());
	}
	
	/**
	 * @return Point representing the direction of gecko
	 */
	public Point getDirection()
	{
		return super.getDirection();
	}
	
	/**
	 * Sets up the following gecko
	 */
	public void addFollower(DancingGecko fo)
	{
		follower = fo;
		fo.spinRight(); //The gecko is facing the opposite direction (this code turns him 180 degrees)
		fo.spinRight();
		fo.moveForward(-GeckoConstants.BLOCK_DIST);
	}
	
	/**
	 * Removes the existing follower
	 * @param dg The following Dancing Gecko
	 */
	public void removeFollower(DancingGecko dg)
	{
		follower = null;
	}

	/**
	 * Moves the gecko along with it's Followers
	 */
	public void step() 
	{
		boolean canMoveBack;
		if(walkingForwards)
		{
			 canMoveBack = follower.moveForward(-GeckoConstants.BLOCK_DIST);
			if(!canMoveBack)
				walkingForwards = false;
			else
				super.moveForward(GeckoConstants.BLOCK_DIST);
		}
		else
		{
			 canMoveBack = super.moveForward(-GeckoConstants.BLOCK_DIST);
			 if(!canMoveBack)
				 walkingForwards = true;
			else
				follower.moveForward(GeckoConstants.BLOCK_DIST);
		}
	}


}


/*
 * L
 * F
 */
