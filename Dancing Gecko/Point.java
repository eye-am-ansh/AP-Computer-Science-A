/**
 * Point.java
 * 
 * A simple Point class, with x and y values, to indicate
 * the direction of the DancingGecko.
 *
 * @author Scott DeRuiter, Ansh Chaurasia
 * @version 2.0
 * @since 11/21/2019
 */

public class Point
{
	/**Represents the two pair of values (used for x/y or direction left-right / up-down) */
	private int x, y;
	
	public Point(int x, int y)
	{
		this.x = x;
		this.y = y;
	}
	
	/**
	 * @return The first value (x or left-right)
	 */
	public int getX ( )
	{
		return x;
	}

	/**
	 * @return The second value (y or up-down)
	 */
	public int getY ( )
	{
		return y;
	}
	

	public void setX(int x) //Sets the value of x
	{
		this.x = x;
	}
	
	public void setY(int y) //Sets value of variable y
	{
		this.y = y;
	}

}
