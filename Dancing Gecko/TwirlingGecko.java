/**
 * TwirlingGecko.java
 * 
 * Gecko that does the "twirl" dance. (Turning in a specific direction)
 *
 * @author Ansh Chaurasia
 * @version 2.0
 * @since 11/21/2019
 */

import java.awt.Color;
public class TwirlingGecko extends DancingGecko
{

	/**
	 * Regular Constructor
	 * @param x X-Position
	 * @param y Y-Position
	 * @param c Color of the Background
	 */
	public TwirlingGecko(int x, int y, Color c)
	{
		super(x,y,c);
	}
	
	/**
	 * Regular Constructor
	 * @param x X-Position
	 * @param y Y-Position
	 * @param c Color of the Background
	 * @param p The point that represents direction of the gecko
	 */
	public TwirlingGecko(int x, int y, Color c, Point p)
	{
		super(x,y,c,p);
	}
	
	/**
	 * Simply turns right at every step
	 */
	public void step()
	{
		super.spinRight();
	}

}

