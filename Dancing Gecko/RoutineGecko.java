/**
 * RoutineGecko.java
 * 
 * Gecko that does the routine dance, specified by the programmer
 *
 * @author Ansh Chaurasia
 * @version 2.0
 * @since 11/21/2019
 */

import java.awt.Color;
public class RoutineGecko extends DancingGecko
{
	/**Represents the list of steps and the order that the gecko needs to perform them in */
	private GeckoConstants.DanceStep steps [];

	/**Keeps track of the current step */
	private int currentStep;

	/**
	 * Regular Constructor, with ability to take in dance steps as an argument
	 * @param x X-Position
	 * @param y Y-Position
	 * @param c Color of the Background
	 * @param dc List of dance steps
	 */
	public RoutineGecko(int x, int y, Color c, GeckoConstants.DanceStep gc [])
	{
		super(x,y,c);
		steps = gc;
		currentStep = 0;
	}
	
	/**
	 * Constructor that immiedietly calls the super class
	 * @param x X-Position
	 * @param y Y-Position
	 * @param c Color of the Background
	 * @param p Point that represents the direction the gecko is facing
	 * @param dc List of dance steps
	 */
	public RoutineGecko(int x, int y, Color c, Point p, GeckoConstants.DanceStep gc [])
	{
		super(x,y,c,p);
		steps = gc;
		currentStep = 0;
	}
	
	/**
	 * Handles a single step
	 */
	public void step()
	{
		processStep(this);
		currentStep = (currentStep + 1) % steps.length;
	}
	
	/**
	 * Handles each dance move, based on the current enumerated constant.
	 */
	public void processStep(DancingGecko dg)
	{
		switch(steps[currentStep])
		{
			case FORWARD:
				dg.moveForward(GeckoConstants.BLOCK_DIST);
				break;
			case BACKWARD:
				dg.moveForward(-GeckoConstants.BLOCK_DIST);
				break;
			case RIGHT:
				dg.moveSide(GeckoConstants.BLOCK_DIST);
				break;
			case LEFT:
				dg.moveSide(-GeckoConstants.BLOCK_DIST);
				break;
			case TURN_RIGHT:
				dg.spinRight();
				break;
			case TURN_LEFT:
				dg.spinLeft();
				break;
			default:
				break;
			
			
		}
			
		
	}

}

