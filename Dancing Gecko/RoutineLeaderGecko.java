/**
 * RoutineLeaderGecko.java
 * 
 * Gecko that has the ability to perform a "dance floor routine"!
 * Performing a series of steps, it is paired with other geckos 
 * to create an action packed experience.
 * 
 * @author Ansh Chaurasia
 * @version 2.0
 * @since 11/21/2019
 */
import java.awt.Color;
public class RoutineLeaderGecko extends RoutineGecko implements Leader
{
	/**Represents the follower geckos that follow it's steps */
	private DancingGecko followers []; 

	/**Represents the direction (positive / negative respectively represents forward / backward) of the gecko*/
	private int direction = 1;
	
	/**Represents the current size of the follower gecko array (Number of geckos in the array) in format that models the ArrayList*/
	private int size = 0;

	/**Represents the direction (positive / negative) of the gecko*/	
	private final int CAPACITY = 20;

	/**
	 * Regular Constructor, with ability to take in dance steps as an argument
	 * @param x X-Position
	 * @param y Y-Position
	 * @param c Color of the Background
	 * @param dc List of dance steps
	 */
	public RoutineLeaderGecko(int x, int y, Color c,GeckoConstants.DanceStep [] dc)
	{
		super(x,y,c,dc);
		init();		
	}
	

	/**
	 * Constructor that immiedietly calls the super class
	 * @param x X-Position
	 * @param y Y-Position
	 * @param c Color of the Background
	 * @param p Point that represents the direction the gecko is facing
	 * @param dc List of dance steps
	 */
	public RoutineLeaderGecko(int x, int y, Color c, Point p,GeckoConstants.DanceStep [] dc )
	{
		super(x,y,c,p,dc);
		init();
	}

	/**
	 * "Upon Initialization" method to set any necessary variables and to prevent repeated code
	 */
	public void init()
	{
		setStringLabel("L");
		followers = new FollowingGecko [CAPACITY];
	}
	
	/**
	 * @return The location of the object, in point format
	 */
	public Point getLocation()
	{
		return new Point(super.getX(),super.getY());
	}
	
	/**
	 * @return The direction of the object, in point format
	 */
	public Point getDirection()
	{
		return super.getDirection();
	}
	
	public void addFollower(DancingGecko fo) //Simulating ArrayList add method 
	{
		if(followers == null)
			init();
				
		followers[size++] = fo;
		
	}
	
	public void removeFollower(DancingGecko dg) //Simulating ArrayList remove method
	{
		if(followers == null)
			init();

		followers[size--] = null;
	}

	/**
	 * @return The size of the follower array
	 */
	public int getSize() 
	{
		return size;
	}

	/**
	 * Performs a step, along with moving all the following geckos.
	 * Order:
	 * 1. Move all Geckos
	 * 2. Move this Gecko
	 */
	public void step() 
	{
		for(int i = 0; i < getSize(); i++)	 
			super.processStep(followers[i]);
		super.step();
	}
}
