/**
 * FollowingGecko.java
 * 
 * @author Scott DeRuiter
 * @version 1.0
 * @since 11/12/2019
 */

import java.awt.Color;
public class FollowingGecko extends DancingGecko
{
	private Leader lead;
	public FollowingGecko(Leader le, Color co)
	{
		super(le.getLocation().getX(),le.getLocation().getY(),co,le.getDirection());
		lead = le;
		init();
	}

	public FollowingGecko(Leader le, int x, int y, Color c, Point dir)
	{	
		super(x,y,c,dir);
		lead = le;
		init();
		
	}
	
	
	public void init()
	{
		lead.addFollower(this);
		setStringLabel("F");
	}
	
	public void step() 
	{ }
}
