/**
 * SideToSideGecko.java
 * 
 * Gecko that does the "Side to Side" dance
 *
 * @author Ansh Chaurasia
 * @version 2.0
 * @since 11/21/2019
 */

import java.awt.Color;
public class SideToSideGecko extends DancingGecko
{
	/**Keeps track of the current move */
	private int move;

	/**
	 * Regular Constructor
	 * @param x X-Position
	 * @param y Y-Position
	 * @param c Color of the Background
	 */
	public SideToSideGecko(int x, int y, Color c)
	{
		super(x,y,c);
		move = GeckoConstants.BLOCK_DIST;
	}
	
	/**
	 * Regular Constructor
	 * @param x X-Position
	 * @param y Y-Position
	 * @param c Color of the Background
	 * @param p The point that represents direction of the gecko
	 */
	public SideToSideGecko(int x, int y, Color c, Point p)
	{
		super(x,y,c,p);
		move = GeckoConstants.BLOCK_DIST;
	}
	
	/**
	 * Moves the gecko forward / backward, depending on the previous move
	 */
	public void step()
	{
		super.moveSide(move);
		move *= -1;
	}

}

