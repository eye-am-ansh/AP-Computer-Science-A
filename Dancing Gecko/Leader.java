/**
 * Leader.java
 * 
 * @author Scott DeRuiter
 * @version 1.0
 * @since 11/12/2019
 */

import java.awt.Color;

public interface Leader
{
	public Point getLocation();
	public Point getDirection();
	public void addFollower(DancingGecko dg);
	public void removeFollower(DancingGecko dg);
}
