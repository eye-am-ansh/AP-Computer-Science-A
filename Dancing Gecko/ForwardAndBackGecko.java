/**
 * ForwardAndBackGecko.java
 * 
 * Gecko that does the "Forward and Backward" dance
 *
 * @author Ansh Chaurasia
 * @version 1.0
 * @since 11/12/2019
 */

import java.awt.Color;
public class ForwardAndBackGecko extends DancingGecko
{
	/**Represents the current dance step, direction (forward / backwards) */
	private int move;

	
	public ForwardAndBackGecko(int x, int y, Color c)
	{
		super(x,y,c);
		move = GeckoConstants.BLOCK_DIST;
	}
	
	public ForwardAndBackGecko(int x, int y, Color c, Point p)
	{
		super(x,y,c,p);
		move = GeckoConstants.BLOCK_DIST;
	}
	
	public void step()
	{
		super.moveForward(move);
		move *= -1;
	}

}

